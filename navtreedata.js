/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Matthew's Portfolio", "index.html", [
    [ "Table of Contents", "index.html", [
      [ "ME 305: Introduction to Mechatronics", "index.html#ME305Intro", null ],
      [ "ME 405: Mechatronics", "index.html#ME405Intro", null ],
      [ "ME 507: Mechanical Control System Design", "index.html#ME507Intro", null ]
    ] ],
    [ "405 Lab 0x02: Reaction Times", "405_lab0x02.html", [
      [ "Part A: Software Based Methodology", "405_lab0x02.html#partA", null ],
      [ "Part B: Hardware Based Methodology", "405_lab0x02.html#partB", null ],
      [ "Demonstration Videos", "405_lab0x02.html#lab2_demoVideos", null ],
      [ "Discussion Questions", "405_lab0x02.html#lab2_discussionQuestions", null ]
    ] ],
    [ "405 Lab 0x03: Pushing the Right Buttons", "405_lab0x03.html", [
      [ "Project Description:", "405_lab0x03.html#description", null ]
    ] ],
    [ "405 Lab 0x04: Hot or Not?", "405_lab0x04.html", [
      [ "Lab Description:", "405_lab0x04.html#description04", null ],
      [ "Code Implementation", "405_lab0x04.html#codeLab0x04", null ]
    ] ],
    [ "405 Lab 0xFF: Final Project", "405_lab0xFF.html", "405_lab0xFF" ],
    [ "405 Lab 0x01: VendoTron Finite State Machine", "ME405_Lab0x01.html", null ],
    [ "507 Statistically Significant HW: Classes in C++", "507_StatSignificant.html", null ],
    [ "305 Lab 0xFF: Final Project", "ME305_Lab0xFF.html", "ME305_Lab0xFF" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"405Lab0x01__page_8py.html",
"classdataTask_1_1dataTask.html#af7eb50cf28836fe0d117b0a354cfd663",
"functions_r.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';