var classuiTaskLab0x03_1_1uiTask =
[
    [ "__init__", "classuiTaskLab0x03_1_1uiTask.html#a55cda27ff1abd22bca346b1d21f5108e", null ],
    [ "run", "classuiTaskLab0x03_1_1uiTask.html#ac816f911d3f604987ece522fa606b9f2", null ],
    [ "transitionTo", "classuiTaskLab0x03_1_1uiTask.html#a888c1f2d0ea0b2d9c8d2a61bab062d11", null ],
    [ "adc", "classuiTaskLab0x03_1_1uiTask.html#aeb330902b9045aae13a42560b701a7b5", null ],
    [ "debugFlag", "classuiTaskLab0x03_1_1uiTask.html#a32e82df29c68916be6175919b990089a", null ],
    [ "differenceInVoltage", "classuiTaskLab0x03_1_1uiTask.html#a1dc29386190cb6eb27d327df5812ac00", null ],
    [ "n", "classuiTaskLab0x03_1_1uiTask.html#a3939357e1c12a5a43846bf66d3202832", null ],
    [ "sendingDataFlag", "classuiTaskLab0x03_1_1uiTask.html#a43bf327ecd0524623cecc376c9a9f056", null ],
    [ "state", "classuiTaskLab0x03_1_1uiTask.html#a23d6b048ccceb9f0f98b1fffe58ea1e1", null ],
    [ "tim", "classuiTaskLab0x03_1_1uiTask.html#a9c21dccb7c4dfaa2e5c1ff197ea34c5c", null ],
    [ "val", "classuiTaskLab0x03_1_1uiTask.html#a8c9bdea6566169057c87d52ecbeaffdb", null ],
    [ "voltages", "classuiTaskLab0x03_1_1uiTask.html#a0c77db700e32a6101d5736c7b1e2fdeb", null ]
];