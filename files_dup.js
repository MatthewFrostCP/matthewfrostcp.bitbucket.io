var files_dup =
[
    [ "405_Lab0x01.py", "405__Lab0x01_8py.html", "405__Lab0x01_8py" ],
    [ "405_Lab0x02_mainPage.py", "405__Lab0x02__mainPage_8py.html", null ],
    [ "405_Lab0x03_mainPage.py", "405__Lab0x03__mainPage_8py.html", null ],
    [ "405_Lab0x04_mainPage.py", "405__Lab0x04__mainPage_8py.html", null ],
    [ "405_Lab0xFF_Page.py", "405__Lab0xFF__Page_8py.html", null ],
    [ "405Lab0x01_page.py", "405Lab0x01__page_8py.html", null ],
    [ "507StatSignHW_page.py", "507StatSignHW__page_8py.html", null ],
    [ "controllerDriver.py", "controllerDriver_8py.html", [
      [ "controllerDriver.controllerDriver", "classcontrollerDriver_1_1controllerDriver.html", "classcontrollerDriver_1_1controllerDriver" ]
    ] ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "ctrlTask.py", "ctrlTask_8py.html", [
      [ "ctrlTask.ctrlTask", "classctrlTask_1_1ctrlTask.html", "classctrlTask_1_1ctrlTask" ]
    ] ],
    [ "dataTask.py", "dataTask_8py.html", "dataTask_8py" ],
    [ "Elevator_Example.py", "Elevator__Example_8py.html", "Elevator__Example_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "encoderDriver.py", "encoderDriver_8py.html", [
      [ "encoderDriver.encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", "encoderTask_8py" ],
    [ "Fibonacci_Calculator.py", "Fibonacci__Calculator_8py.html", "Fibonacci__Calculator_8py" ],
    [ "game.py", "game_8py.html", [
      [ "game.game", "classgame_1_1game.html", "classgame_1_1game" ]
    ] ],
    [ "Lab0x02.py", "Lab0x02_8py.html", "Lab0x02_8py" ],
    [ "Lab0x02_A.py", "Lab0x02__A_8py.html", "Lab0x02__A_8py" ],
    [ "Lab0x02_B.py", "Lab0x02__B_8py.html", "Lab0x02__B_8py" ],
    [ "Lab0x03_main.py", "Lab0x03__main_8py.html", null ],
    [ "Lab0x03_PC_Frontend.py", "Lab0x03__PC__Frontend_8py.html", "Lab0x03__PC__Frontend_8py" ],
    [ "Lab0x03_uiTask.py", "Lab0x03__uiTask_8py.html", "Lab0x03__uiTask_8py" ],
    [ "Lab0xFF_page.py", "Lab0xFF__page_8py.html", null ],
    [ "main.cpp", "main_8cpp.html", null ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808.mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.motorDriver", "classmotor_1_1motorDriver.html", "classmotor_1_1motorDriver" ]
    ] ],
    [ "motorDriver.py", "motorDriver_8py.html", [
      [ "motorDriver.motorDriver", "classmotorDriver_1_1motorDriver.html", "classmotorDriver_1_1motorDriver" ]
    ] ],
    [ "motorDriverChannel.py", "motorDriverChannel_8py.html", [
      [ "motorDriverChannel.motorDriverChannel", "classmotorDriverChannel_1_1motorDriverChannel.html", "classmotorDriverChannel_1_1motorDriverChannel" ]
    ] ],
    [ "motorTask.py", "motorTask_8py.html", [
      [ "motorTask.motorTask", "classmotorTask_1_1motorTask.html", "classmotorTask_1_1motorTask" ]
    ] ],
    [ "nucleoClass.py", "nucleoClass_8py.html", "nucleoClass_8py" ],
    [ "PC_FrontEnd.py", "PC__FrontEnd_8py.html", "PC__FrontEnd_8py" ],
    [ "PCFrontEnd.py", "PCFrontEnd_8py.html", "PCFrontEnd_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "stat-tracker.cpp", "stat-tracker_8cpp.html", null ],
    [ "stat-tracker.h", "stat-tracker_8h.html", [
      [ "StatTracker", "classStatTracker.html", "classStatTracker" ]
    ] ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touchPanelCalibration_main.py", "touchPanelCalibration__main_8py.html", "touchPanelCalibration__main_8py" ],
    [ "touchPanelDriver.py", "touchPanelDriver_8py.html", [
      [ "touchPanelDriver.touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "touchPanelTask.py", "touchPanelTask_8py.html", [
      [ "touchPanelTask.touchPanelTask", "classtouchPanelTask_1_1touchPanelTask.html", "classtouchPanelTask_1_1touchPanelTask" ]
    ] ],
    [ "uiTask.py", "uiTask_8py.html", "uiTask_8py" ]
];