var classStatTracker =
[
    [ "StatTracker", "classStatTracker.html#a8966aabbd4a2eaddd975fcfc137c2a07", null ],
    [ "add_data", "classStatTracker.html#a72e03236a40776e3cd05fb93e997a3fd", null ],
    [ "add_data", "classStatTracker.html#a134422d96a19576cccbc7d072258323a", null ],
    [ "add_data", "classStatTracker.html#a91ecbad6e987919ebba0e0330afcfa16", null ],
    [ "average", "classStatTracker.html#a8c0d201e498e31cbdf42e21cac5200e8", null ],
    [ "clear", "classStatTracker.html#a39f4777ec147f38013110a419d1287db", null ],
    [ "num_points", "classStatTracker.html#a9c58a53a361938e03690c6908d2399e7", null ],
    [ "std_dev", "classStatTracker.html#a3418a2c1e71561b48b40badc59d6500b", null ],
    [ "numPoints", "classStatTracker.html#aa91481e57f90abde55bf490fc0979703", null ],
    [ "sum", "classStatTracker.html#aa392efff616ccca488125fbfe37c0c59", null ],
    [ "sumSquared", "classStatTracker.html#aea4fb6986b1287f0c2647d08291bb945", null ],
    [ "Sx", "classStatTracker.html#a966479f87ff8de2dfc77afadf4128b1f", null ]
];