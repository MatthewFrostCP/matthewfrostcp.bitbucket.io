var searchData=
[
  ['week_201_3a_20familiarization_20with_20serial_20communication_0',['Week 1: Familiarization with Serial Communication',['../Week1.html',1,'ME305_Lab0xFF']]],
  ['week_202_3a_20introduction_20to_20quadrature_20encoders_1',['Week 2: Introduction to Quadrature Encoders',['../Week2.html',1,'ME305_Lab0xFF']]],
  ['week_203_3a_20introduction_20to_20p_20controller_20for_20spinning_20motors_2',['Week 3: Introduction to P Controller for Spinning Motors',['../Week3.html',1,'ME305_Lab0xFF']]],
  ['week_204_3a_20implementing_20a_20pi_20controller_20for_20tracking_20purposes_3',['Week 4: Implementing a PI Controller for Tracking Purposes',['../Week4.html',1,'ME305_Lab0xFF']]],
  ['wintime_4',['winTime',['../classgame_1_1game.html#a32b082b9bdc6e650b0734959d52c4422',1,'game::game']]],
  ['writetocsv_5',['writeToCSV',['../Lab0x03__PC__Frontend_8py.html#a20b55d29dabe14d2be4b9e65074117b9',1,'Lab0x03_PC_Frontend.writeToCSV()'],['../PC__FrontEnd_8py.html#a055445ecb3433c2ecc60350d17bc8504',1,'PC_FrontEnd.writeToCSV()'],['../PCFrontEnd_8py.html#a69b92718b062ce5eca27eab47116f9a6',1,'PCFrontEnd.writeToCSV()']]]
];
