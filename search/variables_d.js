var searchData=
[
  ['n_0',['n',['../classLab0x03__uiTask_1_1uiTask.html#a7556f34be9e1a330d67acafc48ac16f9',1,'Lab0x03_uiTask::uiTask']]],
  ['name_1',['name',['../classcotask_1_1Task.html#ab54e069dd0b4f0a2f8e7f00c94998a10',1,'cotask::Task']]],
  ['newcompareval_2',['newCompareVal',['../Lab0x02__B_8py.html#a5c23f147a0169ef7e1f00a9bb7aba00e',1,'Lab0x02_B']]],
  ['newl_3',['newL',['../classcontrollerDriver_1_1controllerDriver.html#a00392972f773d6d5adee55712da6f8b7',1,'controllerDriver::controllerDriver']]],
  ['newround_4',['newRound',['../classgame_1_1game.html#a772719e6c96018caaaa7811166011ebd',1,'game::game']]],
  ['nexttime_5',['nextTime',['../classmcp9808_1_1mcp9808.html#a62252a12743b5861b72f2bc039a74ad5',1,'mcp9808::mcp9808']]],
  ['nfault_5fpin_6',['nFAULT_pin',['../classmotorDriver_1_1motorDriver.html#a33511c0efd8c2dc6cdf7c7e62bf7c903',1,'motorDriver::motorDriver']]],
  ['nsleep_5fpin_7',['nSLEEP_pin',['../classmotor_1_1motorDriver.html#ab34b99bc37c78c0535daead8f66178a4',1,'motor.motorDriver.nSLEEP_pin()'],['../classmotorDriver_1_1motorDriver.html#a8d0c882f9eaf9e382862ff50d5a76343',1,'motorDriver.motorDriver.nSLEEP_pin()']]],
  ['nucleoarray_8',['nucleoArray',['../classmcp9808_1_1mcp9808.html#a5ab5724e6611e1f4cfc53452c80ecff8',1,'mcp9808::mcp9808']]],
  ['nucleoscore_9',['nucleoScore',['../classgame_1_1game.html#a631f8afabfa6f6e2022f53634e064b02',1,'game::game']]],
  ['numpoints_10',['numPoints',['../classStatTracker.html#aa91481e57f90abde55bf490fc0979703',1,'StatTracker']]],
  ['numsamples_11',['numSamples',['../classmcp9808_1_1mcp9808.html#a9ddbf9d3821de54302a9580876b26452',1,'mcp9808::mcp9808']]],
  ['numtests_12',['numTests',['../touchPanelCalibration__main_8py.html#a4a18b5c76bd85210434b74da40bcc1ac',1,'touchPanelCalibration_main']]]
];
