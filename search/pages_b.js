var searchData=
[
  ['week_201_3a_20familiarization_20with_20serial_20communication_0',['Week 1: Familiarization with Serial Communication',['../Week1.html',1,'ME305_Lab0xFF']]],
  ['week_202_3a_20introduction_20to_20quadrature_20encoders_1',['Week 2: Introduction to Quadrature Encoders',['../Week2.html',1,'ME305_Lab0xFF']]],
  ['week_203_3a_20introduction_20to_20p_20controller_20for_20spinning_20motors_2',['Week 3: Introduction to P Controller for Spinning Motors',['../Week3.html',1,'ME305_Lab0xFF']]],
  ['week_204_3a_20implementing_20a_20pi_20controller_20for_20tracking_20purposes_3',['Week 4: Implementing a PI Controller for Tracking Purposes',['../Week4.html',1,'ME305_Lab0xFF']]]
];
