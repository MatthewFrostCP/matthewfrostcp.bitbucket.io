var ME305_Lab0xFF =
[
    [ "Week 1: Familiarization with Serial Communication", "Week1.html", null ],
    [ "Week 2: Introduction to Quadrature Encoders", "Week2.html", null ],
    [ "Week 3: Introduction to P Controller for Spinning Motors", "Week3.html", null ],
    [ "Week 4: Implementing a PI Controller for Tracking Purposes", "Week4.html", null ]
];