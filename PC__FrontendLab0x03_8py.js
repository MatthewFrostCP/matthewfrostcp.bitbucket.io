var PC__FrontendLab0x03_8py =
[
    [ "clearFiles", "PC__FrontendLab0x03_8py.html#a70721136c281539c1a4b64a7826c4a01", null ],
    [ "getTimeConstant", "PC__FrontendLab0x03_8py.html#aaf4d8f7a19976716f69bcfa36480f33e", null ],
    [ "kb_cb", "PC__FrontendLab0x03_8py.html#a7b22395302575df89cb92067d5ee199e", null ],
    [ "processData", "PC__FrontendLab0x03_8py.html#ac3a03a4aabdad16efaeee1fc7355d762", null ],
    [ "sendChar", "PC__FrontendLab0x03_8py.html#a954f817ad143ef05b8143f8a28f35cfa", null ],
    [ "writeToCSV", "PC__FrontendLab0x03_8py.html#a41c5a386b5c8481b92d8f15ffd9e683c", null ],
    [ "_thisKey", "PC__FrontendLab0x03_8py.html#a072fcc5446684fc82221cace69bece3b", null ],
    [ "callback", "PC__FrontendLab0x03_8py.html#af9145735116bc8e141d9c37d7fe17d84", null ],
    [ "callbackKeys", "PC__FrontendLab0x03_8py.html#aa675455616826433f6a7e10db3a16695", null ],
    [ "csvInitial", "PC__FrontendLab0x03_8py.html#a965b68dddff9ebb7eb093794598bd619", null ],
    [ "csvModified", "PC__FrontendLab0x03_8py.html#a00a068c466c6f44db04a6b754db742cc", null ],
    [ "currentFromNucleo", "PC__FrontendLab0x03_8py.html#a1b0db7f7fe694e3149e6baaa240382e9", null ],
    [ "currentTau", "PC__FrontendLab0x03_8py.html#a5dc70739af6193ea5983394a218fcc16", null ],
    [ "dataFromNucleo", "PC__FrontendLab0x03_8py.html#a0fea7ecdc37277db9152bfa8657f091b", null ],
    [ "debugFlag", "PC__FrontendLab0x03_8py.html#a18321fd2be4ee0b9fad66cc77996ccbd", null ],
    [ "introMessage", "PC__FrontendLab0x03_8py.html#a3acf3f74a65debb0e6f2bf3877808d0b", null ],
    [ "last_key", "PC__FrontendLab0x03_8py.html#aa7b9283abb4fee725aa33bd9515840e3", null ],
    [ "pngInitial", "PC__FrontendLab0x03_8py.html#a512c8da686cdba481a5cdfbb597fe9cc", null ],
    [ "pngLog", "PC__FrontendLab0x03_8py.html#aa705e9648f8d3c89a9fa1a1805488684", null ],
    [ "pngModified", "PC__FrontendLab0x03_8py.html#abe6908e50f72f1cc93cd4ff9b36381c3", null ],
    [ "ser", "PC__FrontendLab0x03_8py.html#a083d3a78d5ce98926cfd8240e11f86bc", null ],
    [ "succesfulProcessing", "PC__FrontendLab0x03_8py.html#a371f9b8f71b6f855a6908fc394ad695c", null ]
];