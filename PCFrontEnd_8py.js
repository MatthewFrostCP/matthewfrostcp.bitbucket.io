var PCFrontEnd_8py =
[
    [ "clearFiles", "PCFrontEnd_8py.html#ad1c5b67f5c93d994f2619a2fe28dcaca", null ],
    [ "plotData", "PCFrontEnd_8py.html#a3fe7a4504186a13d5e214a9107babcc8", null ],
    [ "writeToCSV", "PCFrontEnd_8py.html#a69b92718b062ce5eca27eab47116f9a6", null ],
    [ "csvFileName", "PCFrontEnd_8py.html#ae4e87b7c45fd707dc09881a2fc0a732e", null ],
    [ "currentFromNucleo", "PCFrontEnd_8py.html#af1236d1268be7d75c7d2a1064a8b7b89", null ],
    [ "dataFromDataTask", "PCFrontEnd_8py.html#af6e6f1632ad134f3297d03333f5168d6", null ],
    [ "dataToSave", "PCFrontEnd_8py.html#aaebdc7f5d9e3454dcdea88fe2ca1b4c4", null ],
    [ "debugFlag", "PCFrontEnd_8py.html#af050d7d72b7340895705946c4672f10a", null ],
    [ "pngFileNameX", "PCFrontEnd_8py.html#a52d2752e97a3a70be39c6b9d0431df6d", null ],
    [ "pngFileNameY", "PCFrontEnd_8py.html#a6e2d18a3c41478b449cc1fb32fddf7aa", null ],
    [ "ser", "PCFrontEnd_8py.html#a91679a86a045bfd8940d4b0bf13c2c54", null ]
];